package middlewares

import (
	"net/http"
	"os"
	"strings"
)

func ApiAuth(next http.HandlerFunc) http.HandlerFunc {
	return http.HandlerFunc(func(writer http.ResponseWriter, request *http.Request) {
		auth := request.Header.Get("Authorization")

		authTypePosition := strings.Index(auth, " ")
		authType := auth[0:authTypePosition]
		switch strings.ToLower(authType) {
		case "basic":
			{
				user, password, ok := request.BasicAuth()
				if !ok || user != os.Getenv("BASIC_USER") || password != os.Getenv("BASIC_PASSWORD") {
					http.Error(writer, "Unauthorized", 401)
					return
				}
			}
		default:
			http.Error(writer, "Unauthorized", 401)
			return
		}
		next.ServeHTTP(writer, request)
		return
	})
}
