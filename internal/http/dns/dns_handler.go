package dns

import (
	"encoding/json"
	"gitlab.com/jan.staffel/dns-notifier/internal/cloudflareClient"
	"io"
	"io/ioutil"
	"log"
	"net"
	"net/http"
)

type Handler struct {
}

func NewHandler() Handler {
	return Handler{}
}

type ErrorResponse struct {
	Error string
}

func (handler Handler) UpdateDns(writer http.ResponseWriter, request *http.Request) {
	domain := request.URL.Query().Get("domain")
	ip := request.URL.Query().Get("ip")

	ipValid := validateIp(ip)
	if !ipValid {
		writer.WriteHeader(400)
		message, _ := json.Marshal(ErrorResponse{
			Error: "Invalid field value: ip",
		})
		_, err := writer.Write(message)
		if err != nil {
			log.Fatalf("Error on writing response: %v\n", err)
		}
	}

	dC := cloudflareClient.NewDnsConfiguration(ip, domain)

	ok, response := dC.UpdateIp(ip)

	responseBody, err := ioutil.ReadAll(response.Body)
	if err != nil {
		writer.WriteHeader(400)
		_, err := writer.Write(responseBody)
		log.Fatalf("Error on parsing response: %v", err)
	}

	defer func(Body io.ReadCloser) {
		err := Body.Close()
		if err != nil {
			log.Println("Error on closing request Body")
		}
	}(response.Body)

	if ok == false {
		writer.WriteHeader(400)
		_, err := writer.Write(responseBody)
		if err != nil {
			log.Fatalf("Error on writing response: %v\n", err)
		}
	}

	writer.WriteHeader(200)
	_, err = writer.Write(responseBody)
	if err != nil {
		log.Fatalf("Error on writing response: %v\n", err)
	}
}

func validateIp(ip string) bool {
	if net.ParseIP(ip) == nil {
		return false
	}
	return true
}
