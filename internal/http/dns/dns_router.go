package dns

import (
	"github.com/gorilla/mux"
)

func BuildRoutes(parentRouter *mux.Router) {
	dnsHandler := NewHandler()
	dnsRouter := parentRouter.PathPrefix("/dns").Subrouter()
	dnsRouter.Methods("GET").Path("/update").HandlerFunc(dnsHandler.UpdateDns).Name("updatedns")

}
