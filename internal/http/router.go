package http

import (
	"github.com/gorilla/mux"
	"gitlab.com/jan.staffel/dns-notifier/internal/http/dns"
	"net/http"
)

type AppRouter struct {
	mux *mux.Router
	//	middlewarestack []Middleware
}

func (a Application) ServeHTTP(writer http.ResponseWriter, request *http.Request) {
	a.GlobalAppRouter.mux.ServeHTTP(writer, request)
}

type Application struct {
	GlobalAppRouter *AppRouter
}

func New() http.Handler {
	router := AppRouter{
		mux: mux.NewRouter(),
		//		middlewarestack: nil,
	}

	registerRoutes(router.mux)
	app := Application{GlobalAppRouter: &router}

	return app
}

func registerRoutes(router *mux.Router) {
	dns.BuildRoutes(router)
}
