package cloudflareClient

import (
	"bytes"
	"encoding/json"
	"log"
	"net/http"
	"os"
)

type dnsConfiguration struct {
	ip               string
	domain           string
	zoneIdentifier   string
	recordIdentifier string
}

type cloudFlareApi interface {
	UpdateIp(ip string) (ok bool, response *http.Response)
}

func NewDnsConfiguration(ip string, domain string) *dnsConfiguration {
	dC := dnsConfiguration{
		ip:     ip,
		domain: domain,
	}
	return &dC
}

func (dC *dnsConfiguration) UpdateIp(ip string) (ok bool, response *http.Response) {
	cloudflareToken, exist1 := os.LookupEnv("CLOUDFARE_TOKEN")

	if exist1 == false {
		log.Fatal("Necessary secret missing")
	}

	body, _ := json.Marshal(map[string]string{
		"name":    dC.domain,
		"content": ip,
		"ttl":     "1",
		"type":    "A",
	})
	requestBody := bytes.NewBuffer(body)

	cloudflareClient := &http.Client{}

	request, err := http.NewRequest(
		"PATCH",
		"https://api.cloudflare.com/client/v4/zones/"+dC.zoneIdentifier+"/dns_records/"+dC.recordIdentifier,
		requestBody)

	if err != nil {
		log.Printf("Error on creating Request.\nError: %v", err)
		return false, nil
	}

	request.Header.Add("Authorization", "Bearer "+cloudflareToken)

	clientResponse, err := cloudflareClient.Do(request)

	if err != nil {
		log.Printf("Error on updating DNS.\nError: %v", err)
		return false, nil
	}

	if clientResponse.StatusCode != 200 {
		return false, clientResponse
	}

	return true, clientResponse
}
