package nginxconfigbuilder

type configBuilder interface {
	reset()
	getConfig() (string, error)
	checkRequiredFields() error
	getExampleConfig() string
}
