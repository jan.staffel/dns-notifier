package nginxconfigbuilder

import (
	"github.com/pkg/errors"
	"os"
	"regexp"
)

var fileError = errors.New("file Error")
var noPlaceholderInTemplateError = errors.New("no Placeholder in Template")
var placeholdersLeftError = errors.New("placeholders left after filling")

func build() (string, error) {
	cu := getConfigfileUtils()
	file, err := os.ReadFile("templates/https_config")
	if err != nil {
		return "", fileError
	}

	placeholderPattern := regexp.MustCompile(`<<[a-zA-Z_]*>>`)

	placeholdersExist := placeholderPattern.Match(file)
	if !placeholdersExist {
		return "", noPlaceholderInTemplateError
	}

	replacements := make(map[string][]byte)
	replacements["<<SSL_CERTIFICATE_PATH>>"] = []byte("test")
	replacements["<<SSL_CERTIFICATE_KEY_PATH>>"] = []byte("test2")

	for key, value := range replacements {
		cu.replacePlaceholder(key, value, &file)
	}

	placeholdersExist = placeholderPattern.Match(file)
	if placeholdersExist {
		//		for _, occ := range cu.findPlaceholders(file) {
		//			log.Printf("placeholder left: %s", file[occ[0]:occ[1]])
		//		}
		return "", placeholdersLeftError
	}

	content := "abc"
	return content, nil
}

func getStandardConfig() string {
	return "def"
}
