package nginxconfigbuilder

import (
	"testing"
)

func assertCorrectText(t testing.TB, got, want string) {
	t.Helper()
	if got != want {
		t.Errorf("\nhave %q\nwant %q", got, want)
	}
}

func TestHttpsConfig(t *testing.T) {
	t.Run("Build config", func(t *testing.T) {
		cb := NewHttpsConfigBuilder()
		cb.setServerName("example.com")
		cb.configureProxy("192.168.178.1")
		got, err := cb.getConfig()
		want := cb.getExampleConfig()
		if err != nil {
			t.Fatalf("Error: %v", err)
		}
		assertCorrectText(t, got, want)
	})
}
