package nginxconfigbuilder

import (
	"github.com/pkg/errors"
	"log"
	"os"
	"strings"
)

type httpsConfigBuilder struct {
	requiredVariables map[string]string
	configFile        *[]byte
	configFileUtil    *configfileUtils
}

func NewHttpsConfigBuilder() *httpsConfigBuilder {
	file, err := os.ReadFile("templates/https_config")
	if err != nil {
		panic(err)
	}

	cb := httpsConfigBuilder{
		requiredVariables: map[string]string{
			serverConfig: "",
			serverName:   "",
		},
		configFile:     &file,
		configFileUtil: getConfigfileUtils(),
	}
	return &cb
}

func (cb *httpsConfigBuilder) reset() {
	for key, _ := range cb.requiredVariables {
		delete(cb.requiredVariables, key)
	}
}

func (cb *httpsConfigBuilder) setServerName(servername string) {
	cb.requiredVariables[serverName] = servername
}

func (cb *httpsConfigBuilder) configureProxy(newProxyPath string) {
	cb.requiredVariables[serverConfig] = cb.configFileUtil.stringReplacePlaceholder(proxyPath, newProxyPath, proxyPass)
}

func (cb httpsConfigBuilder) getConfig() (string, error) {
	configUtil := getConfigfileUtils()
	err := cb.checkRequiredFields()
	if err != nil {
		return "", err
	}

	configFileResult := *cb.configFile

	for key, variable := range cb.requiredVariables {
		configUtil.replacePlaceholder(key, []byte(variable), &configFileResult)
	}

	configUtil.replacePlaceholder(tab, []byte("    "), &configFileResult)
	log.Println("test")
	if configUtil.checkForPlaceholders(&configFileResult) {
		leftstring := ""
		for _, occ := range configUtil.findPlaceholders(&configFileResult) {
			leftstring = leftstring + string(configFileResult[occ[0]:occ[1]])
		}
		return "", errors.Wrap(placeholdersLeftError, leftstring)
	}
	return string(configFileResult), nil
}

func (cb httpsConfigBuilder) checkRequiredFields() error {
	missing := []string{}
	for key, value := range cb.requiredVariables {
		if value == "" {
			missing = append(missing, key)
		}
	}

	if len(missing) > 0 {
		missingString := strings.Join(missing, ", ")
		return errors.New("missing variables: " + missingString)
	}
	return nil
}

func (cb httpsConfigBuilder) getExampleConfig() string {
	file, err := os.ReadFile("templates_examples/httpsConfig_example")
	if err != nil {
		panic(err)
	}
	return string(file)
}
