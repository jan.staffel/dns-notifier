package nginxconfigbuilder

const (
	tab                     string = "<<TAB>>"
	serverConfig            string = "<<SERVER_CONFIG>>"
	serverName              string = "<<SERVER_NAME>>"
	sslCertificatePath      string = "<<SSL_CERTIFICATE_PATH>>"
	sslCertificateKeyPath   string = "<<SSL_CERTIFICATE_KEY_PATH>>"
	caBundleCertificatePath string = "<<CA_BUNDLE_CERTIFICATE_PATH>>"
	nginxTicketKey          string = "<<NGINX_TICKET_KEY>>"
	proxyPath               string = "<<PROXY_PATH>>"
)

const (
	httpBuilderType  string = "httpBuilder"
	httpsBuilderType string = "httpsBuilder"
)

const (
	//double dollars to escape for regex replacement
	redirectToHttps string = `return 301 https://$$server_name$$request_uri;`
	proxyPass       string = "location / {\nproxy_pass <<PROXY_PATH>>;\nproxy_set_header Host $$host;\nproxy_set_header X-Real-IP $$remote_addr;\nproxy_set_header X-Forwarded-For $$proxy_add_x_forwarded_for;\nproxy_set_header Upgrade $$http_upgrade;\n}"
)
