package nginxconfigbuilder

import (
	"regexp"
	"strings"
	"sync"
)

var lock = &sync.Mutex{}

type configfileUtils struct {
	placeholderPattern regexp.Regexp
}

var configfileUtilsInstance *configfileUtils

func getConfigfileUtils() *configfileUtils {
	if configfileUtilsInstance != nil {
		return configfileUtilsInstance
	}
	lock.Lock()
	defer lock.Unlock()
	//second nil check to get rid of race conditions
	if configfileUtilsInstance != nil {
		return configfileUtilsInstance
	}
	configfileUtilsInstance = &configfileUtils{
		placeholderPattern: *regexp.MustCompile(`<<[a-zA-Z_]*>>`),
	}

	return configfileUtilsInstance
}

func (cu configfileUtils) replacePlaceholder(key string, value []byte, text *[]byte) {
	placeholderPattern := regexp.MustCompile(key)
	*text = placeholderPattern.ReplaceAll(*text, value)
}

func (cu configfileUtils) stringReplacePlaceholder(key, value, source string) string {
	return strings.Replace(source, key, value, 1)
}

func (cu configfileUtils) checkForPlaceholders(text *[]byte) bool {
	if cu.placeholderPattern.Match(*text) {
		return true
	}
	return false
}

func (cu configfileUtils) findPlaceholders(text *[]byte) [][]int {
	placeholderOccurences := cu.placeholderPattern.FindAllSubmatchIndex(*text, -1)
	//	for _, tmp := range placeholderOccurences {
	//		log.Println("start: " + fmt.Sprint(tmp[0]) + ". end: " + fmt.Sprint(tmp[1]) + ". content: " + string(text[tmp[0]:tmp[1]]))
	//	}
	return placeholderOccurences
}
