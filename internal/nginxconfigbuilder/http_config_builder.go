package nginxconfigbuilder

import (
	"errors"
	"os"
	"strings"
)

type httpConfigBuilder struct {
	requiredVariables map[string]string
	configFile        *[]byte
	configFileUtil    *configfileUtils
}

func NewHttpConfigBuilder() *httpConfigBuilder {
	file, err := os.ReadFile("templates/http_config")
	if err != nil {
		panic(err)
	}

	cb := httpConfigBuilder{
		requiredVariables: map[string]string{
			serverName:   "",
			serverConfig: "",
		},
		configFile:     &file,
		configFileUtil: getConfigfileUtils(),
	}
	return &cb
}

func (cb *httpConfigBuilder) reset() {
	for key, _ := range cb.requiredVariables {
		delete(cb.requiredVariables, key)
	}
}

func (cb *httpConfigBuilder) configureProxy(newProxyPath string) {
	cb.requiredVariables[serverConfig] = cb.configFileUtil.stringReplacePlaceholder(proxyPath, newProxyPath, proxyPass)
}

func (cb *httpConfigBuilder) configureHttpsRedirect() {
	cb.requiredVariables[serverConfig] = redirectToHttps
}

func (cb *httpConfigBuilder) setServerName(servername string) {
	cb.requiredVariables[serverName] = servername
}

func (cb *httpConfigBuilder) setServerconfig(serverconfig string) {
	cb.requiredVariables[serverconfig] = serverconfig
}

func (cb httpConfigBuilder) getConfig() (string, error) {
	configUtil := getConfigfileUtils()
	err := cb.checkRequiredFields()
	if err != nil {
		return "", err
	}

	configFileResult := *cb.configFile

	for key, variable := range cb.requiredVariables {
		configUtil.replacePlaceholder(key, []byte(variable), &configFileResult)
	}

	configUtil.replacePlaceholder(tab, []byte("    "), &configFileResult)

	if configUtil.checkForPlaceholders(&configFileResult) {
		err = cb.checkRequiredFields()
		return "", err
	}
	return string(configFileResult), nil
}

func (cb httpConfigBuilder) checkRequiredFields() error {
	missing := []string{}
	for key, value := range cb.requiredVariables {
		if value == "" {
			missing = append(missing, key)
		}
	}

	if len(missing) > 0 {
		missingString := strings.Join(missing, ", ")
		return errors.New("missing variables: " + missingString)
	}
	return nil
}
