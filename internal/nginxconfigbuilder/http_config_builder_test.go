package nginxconfigbuilder

import (
	"os"
	"testing"
)

func assertHttpExample(t testing.TB, got, want string) {
	t.Helper()
	if got != want {
		t.Errorf("\nhave %q\nwant %q", got, want)
	}
}

func TestHttpConfig(t *testing.T) {
	t.Run("http redirect config", func(t *testing.T) {
		cb := NewHttpConfigBuilder()
		want, _ := os.ReadFile("templates_examples/httpConfig_redirectHttps_example")
		cb.setServerName("example.com")
		cb.configureHttpsRedirect()
		got, err := cb.getConfig()
		if err != nil {
			t.Fatalf("Error: %v", err)
		}
		assertHttpExample(t, got, string(want))
	})

	t.Run("http proxy config", func(t *testing.T) {
		cb := NewHttpConfigBuilder()
		want, _ := os.ReadFile("templates_examples/httpConfig_proxy_example")
		cb.setServerName("example.com")
		cb.configureProxy("192.168.178.1")
		got, err := cb.getConfig()
		if err != nil {
			t.Fatalf("Error: %v", err)
		}
		assertHttpExample(t, got, string(want))
	})
}
