# DNS Notifier

## Project Purpose
This project provides a deployable proxy to cloudflare. This makes it possible to update DNS entries that are managed by cloudflare via API calls. Since cloudflare provides this service free of charge, we can realise a cost-free, custom DynDNS service supporting wildcards using the DNS Notifier.

This project is especially designed to support the DynDNS feature of an AVM Fritzbox. (https://en.avm.de/products/fritzbox/)

## Current project state
The API provides a single route that takes a FQDN and an IP address and updates the respective DNS entry in cloudflare.
The same can be achieved by invoking the CLI binary from the commandline with FQDN and IP as parameters.

## Usage
The project requires an .env file in the project root for now. Replace the example values by your values configured in the fritzbox and in cloudflare.
Use the makefile to build the project.

### Build the API and CLI binaries
```
make build
```

### Create a docker image that runs the API on port 8080 by default
```
make build_image
```

### Create and run a docker image that runs API on port 8080 by default
```
make run_latest_image
```

### UpdateDNS call (localhost as example)
```
curl --location --request GET 'http://127.0.0.1:8080/updatedns?domain=*.example.com&ip=8.8.8.8' \
--header 'Authorization: Basic Base64Example' \
--header 'Connection: Close'
```

### UpdateDNS cli command (localhost as example)
```
./bin/cli -subdomain *.example.com -ip 8.8.8.8
```
