module gitlab.com/jan.staffel/dns-notifier

go 1.18

require (
	github.com/gorilla/mux v1.8.0
	github.com/joho/godotenv v1.4.0
	github.com/pkg/errors v0.9.1
)
