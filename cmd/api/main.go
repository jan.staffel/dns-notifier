package main

import (
	"context"
	"flag"
	"github.com/joho/godotenv"
	routing "gitlab.com/jan.staffel/dns-notifier/internal/http"
	"log"
	"net/http"
	"os"
	"os/signal"
	"time"
)

func init() {
	errDotEnv := godotenv.Load(".env")
	_, gotEnv := os.LookupEnv("ENV")
	//if there is no .env file present and no ENV Environment Variable set, there are no secrets present
	//TODO: this solution is a little magically because it implies that there is no other environment Variable called ENV, which
	//may work well for containers but may conflict if started on a multi-purpose server
	if errDotEnv != nil && gotEnv == false {
		log.Fatal("Error loading environment")
	}
}

func main() {
	portPtr := flag.String("port", "8080", "Port to serve the webserver on")
	flag.Parse()

	router := routing.New()

	srv := &http.Server{
		Addr:         ":" + *portPtr,
		Handler:      router,
		IdleTimeout:  time.Minute,
		ReadTimeout:  10 * time.Second,
		WriteTimeout: 30 * time.Second,
	}

	log.Println("starting server")

	// Run our server in a goroutine so that it doesn't block.
	go func() {
		if err := srv.ListenAndServe(); err != nil {
			log.Println(err)
			os.Exit(1)
		}
	}()

	c := make(chan os.Signal, 1)
	// We'll accept graceful shutdowns when quit via SIGINT (Ctrl+C)
	// SIGKILL, SIGQUIT or SIGTERM (Ctrl+/) will not be caught.
	signal.Notify(c, os.Interrupt)

	// Block until we receive our signal.
	<-c

	// Create a deadline to wait for.
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	// Doesn't block if no connections, but will otherwise wait
	// until the timeout deadline.
	srv.Shutdown(ctx)
	// Optionally, you could run srv.Shutdown in a goroutine and block on
	// <-ctx.Done() if your application should wait for other services
	// to finalize based on context cancellation.
	log.Println("shutting down")
	os.Exit(0)
}
