package main

import (
	"flag"
	"fmt"
	"github.com/joho/godotenv"
	"gitlab.com/jan.staffel/dns-notifier/internal/cloudflareClient"
	"log"
	"os"
)

func init() {
	errDotEnv := godotenv.Load(".env")
	_, gotEnv := os.LookupEnv("ENV")
	//if there is no .env file present and no ENV Environment Variable set, there are no secrets present
	//TODO: this solution is a little magically because it implies that there is no other environment Variable called ENV, which
	//may work well for containers but may conflict if started on a multi-purpose server
	if errDotEnv != nil && gotEnv == false {
		log.Fatal("Error loading environment")
	}
}

func main() {
	ipPtr := flag.String("ip", "", "New IP to update DNS entry with")
	dnsPtr := flag.String("subdomain", "", "Subdomain that requires IP update")
	flag.Parse()

	log.Println("ip: " + *ipPtr)
	log.Println("subdomain: " + *dnsPtr)

	if *ipPtr == "0.0.0.0" || *dnsPtr == "" {
		fmt.Println("Invalid value for ip or subdomain")
		os.Exit(1)
	}

	dnsConf := cloudflareClient.NewDnsConfiguration(*ipPtr, *dnsPtr)
	dnsConf.UpdateIp(*ipPtr)
}
