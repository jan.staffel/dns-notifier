package main

import (
	"log"
	"os"
)

func setFileLogger() {
	logfile, err := os.OpenFile("log", os.O_WRONLY|os.O_CREATE|os.O_APPEND, 0640)
	defer func(logfile *os.File) {
		err := logfile.Close()
		if err != nil {
			log.Fatal("Error on closing logfile with error: ", err)
		}
	}(logfile)
	if err != nil {
		log.Fatal("Error on opening logfile with error: ", err)
	}
	log.SetOutput(logfile)
}

func setStdoutLogger() {
	log.SetOutput(os.Stdout)
}
