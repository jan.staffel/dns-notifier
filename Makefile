projectname = $(shell basename ${PWD})

build: test vet fmt build

image: test vet fmt build_image

run: image run_latest_image

tmp:
	echo $(projectname)

test:
	go test ./...

vet:
	go vet ./...

fmt:
	go fmt ./...

build:
	go build -ldflags "-s -w" -o bin/api ./cmd/api && chmod +x bin/api
	go build -ldflags "-s -w" -o bin/cli ./cmd/cli && chmod +x bin/cli

build_image:
	docker build -t $(projectname):latest -f build/Dockerfile . && docker image prune --filter label=purpose=go-build-image -f

run_latest_image:
	docker run -it --env-file .env --publish 8080:8080 $(projectname)