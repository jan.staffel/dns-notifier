package handlers

import (
	"io/ioutil"
	"log"
	"net/http"
)

func Flytrap(writer http.ResponseWriter, request *http.Request) {
	body, _ := ioutil.ReadAll(request.Body)
	a, b, c := request.BasicAuth()
	log.Printf("%v\n %v\n %v\n %v, %v, %v\n", request, body, a, b, c, request.URL.Query())
	writer.WriteHeader(404)
	return
}
